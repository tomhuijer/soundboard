// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const fs = require("fs");
const {shell} = require('electron');

const soundsTable = document.getElementById('sounds');
const primaryDeviceSelect = document.getElementById('primaryDevice');
const secondaryDeviceSelect = document.getElementById('secondaryDevice');

const audioPlayers = {
    primary: false,
    secondary: false
};

let storedDevices = JSON.parse(localStorage.getItem('devices'));
if (!storedDevices) {
    storedDevices = {
        primary: false,
        secondary: false
    }
}

function loadSounds() {
    fs.readdir('./sounds', (err, dir) => {
        soundsTable.innerHTML = '';

        for(let filePath of dir) {
            const row = document.createElement('tr');

            const soundLabel = document.createElement('div');
            soundLabel.innerHTML = filePath;

            const soundCell = document.createElement('td');
            soundCell.appendChild(soundLabel)
            row.appendChild(soundCell);

            const playButton = document.createElement('img');
            playButton.src = './assets/img/play.svg';
            playButton.setAttribute('height', '20px');
            playButton.onclick = () => {
                playSound(escape(filePath));
            };

            const playCell = document.createElement('td');
            playCell.appendChild(playButton);
            row.appendChild(playCell);

            soundsTable.appendChild(row);
        }
    });
}
loadSounds();

function loadDevices() {
    navigator.mediaDevices.enumerateDevices()
        .then((devices) => {
            primaryDeviceSelect.innerHTML = '<option value="disabled">Disabled</option>';
            secondaryDeviceSelect.innerHTML = '<option value="disabled">Disabled</option>';

            const audioDevices = devices.filter(device => device.kind === 'audiooutput');
            audioDevices.forEach((device) => {
                const option = document.createElement('option');
                option.value = device.deviceId;
                option.innerHTML = device.label;

                const option2 = option.cloneNode(true);

                if (storedDevices.primary == device.deviceId) {
                    option.setAttribute('selected', 'selected');
                    selectDevice('primary', device.deviceId);
                }

                if (storedDevices.secondary == device.deviceId) {
                    option2.setAttribute('selected', 'selected');
                    selectDevice('secondary', device.deviceId);
                }

                primaryDeviceSelect.appendChild(option);
                secondaryDeviceSelect.appendChild(option2);
            });
        });
}
loadDevices();

function selectDevice(device, value) {
    if (value == 'disabled') {
        audioPlayers[device] = false;
    } else {
        if (!audioPlayers[device]) {
            audioPlayers[device] = new Audio();
        }
        audioPlayers[device].setSinkId(value);
        audioPlayers[device].onended = () => {
            clearPlayer(device);
        };
    }

    storedDevices[device] = value;
    localStorage.setItem('devices', JSON.stringify(storedDevices));
}

function playSound(sound) {
    const path = './sounds/' + sound;

    if (audioPlayers.primary) {
        audioPlayers.primary.src = path;
        audioPlayers.primary.play();
    }

    if (audioPlayers.secondary) {
        audioPlayers.secondary.src = path;
        audioPlayers.secondary.play();
    }
}

function stopSound() {
    if (audioPlayers.primary) {
        clearPlayer('primary');
    }

    if (audioPlayers.secondary) {
        clearPlayer('secondary');
    }
}

function clearPlayer(device) {
    audioPlayers[device].removeAttribute('src');
    audioPlayers[device].load();
}

function openSoundsFolder() {
    shell.openItem(__dirname + '/sounds');
}